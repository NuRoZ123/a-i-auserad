<?php
    include_once ("../xtemplate.class.php");

    $xtpl = new XTemplate("boucle.html");

    $liste = array("1"=>'test Numero 1', "2"=>'test nbr 2', "3"=>'test n°3');

    foreach ($liste as $id => $msg)
    {
        $xtpl->assign("liste", $msg);
        $xtpl->insert_loop("main.boucle", "");
    }

    $xtpl->assign("titre", "test boucle xtpl");
    $xtpl->parse("main");
    $xtpl->out("main");
?>