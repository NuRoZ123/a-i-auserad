<?php

    require("myApp.php");
    include "xtpl/xtemplate.class.php";

    $xtpl = new XTemplate("pages/index.html");

    $users = $application->getUsers();
    $search = "";
    $error = "";
    $message = "";
    $idModif = null;
    $hiddenSearch = "style='display: none'";
    $colorCard = "";
    $selectNom = "";
    $selectPrenom = "";
    $selectPrenomInverse = "";
    $selectNomInverse = "";
    $hiddenOrderBy = "hidden";

    if(sizeof($application->getCurrentUser()->getPermission()) > 0)
    {
        $hiddenSearch = "";
        $hiddenOrderBy = "";
    }

    if($application->getError() != "")
    {
        $error = $application->getError();
        $application->setError("");
    }

    if($application->getMessage() != "")
    {
        $message = $application->getMessage();
        $application->setMessage("");
    }

    if(isset($_POST["search"]))
    {
        $search = $_POST["search"];
    }

    if(isset($_POST["modif"]))
    {
        $searchId = explode(",", $_POST["modif"]);
        $search = $searchId[0];
        $idModif = $searchId[1];
    }

    $isGlobal = false;
    if(isset($_POST["global"]))
    {
        $isGlobal = true;
    }

    if(isset($_GET["orderBy"]))
    {
        if($_GET["orderBy"] != "none")
        {
            if($_GET["orderBy"] == "nom")
            {
                $selectNom = "selected";
            }
            else if($_GET["orderBy"] == "prenom")
            {
                $selectPrenom = "selected";
            }
            else if($_GET["orderBy"] == "prenomReverse")
            {
                $selectPrenomInverse = "selected";
            }
            else if($_GET["orderBy"] == "nomReverse")
            {
                $selectNomInverse = "selected";
            }


            $users = @$application->usersOrderBy($_GET["orderBy"], $search, $users, $isGlobal);

            preg_match("/Reverse/", $_GET["orderBy"], $isReverse);
            if($isReverse)
            {
                $users = array_reverse($users);
            }
        }
        else
        {
            $users = $application->getUsersBySearch($search, $users, $isGlobal);
        }
    }
    else
    {
        $users = $application->getUsersBySearch($search, $users, $isGlobal);
    }

    if(sizeof($users) == 0)
    {
        $error = "Aucun resultat trouvé!";
    }

    if(isset($_POST["modif"]))
    {
        $search = $searchId[1];
    }

    for($i = 0; $i < sizeof($users); ++$i)
    {
        if($users[$i]->getUserAcCtrl() == "514")
        {
            //rouge
//            $colorCard = "#FFECEC";
            $colorCard = "#ED8989";
        }
        else
        {
            //vert
            $colorCard = "#ECFFEC";
        }

        $xtpl->assign("colorCard", $colorCard);
        $xtpl->assign("user", $users[$i]->getListOfThis());
        $xtpl->assign("userAcCtrl", $application->getSelectOptionList()["typeCompte"][$users[$i]->getUserAcCtrl()]);
        $xtpl->assign("modifBTN", $search . "," . $users[$i]->getSamAcName());


        if($idModif == $users[$i]->getSamAcName())
        {
            if(sizeof($application->getCurrentUser()->getPermission()) > 0)
            {
                $typeUser = "RH";
            }
            else
            {
                $typeUser = "utilisateur";
            }

            foreach($application->getModificationList()[$typeUser] as $nom => $valeur)
            {
                if($valeur == "1")
                {
                    if($nom == "societe")
                    {
                        foreach($application->getSelectOptionList()["societe"] as $option)
                        {
                            $xtpl->assign("societe", $option);
                            if($option == $users[$i]->getSociete())
                            {
                                $xtpl->assign("selected", "selected");
                            }
                            else
                            {
                                $xtpl->assign("selected", "");
                            }
                            $xtpl->insert_loop("main.card_modif.input" . ucfirst($nom). ".optionSociete", array());
                        }
                    }

                    if(($nom == "extension1" || $nom == "typeCompte") && sizeof($application->getCurrentUser()->getPermission()) > 0)
                    {
                        foreach($application->getSelectOptionList()[$nom] as $option)
                        {
                            $xtpl->assign($nom, $option);
                            if($option == $users[$i]->getExtension1() || $option == $application->getSelectOptionList()["typeCompte"][$users[$i]->getUserAcCtrl()])
                            {
                                $xtpl->assign("selected", "selected");
                            }
                            else
                            {
                                $xtpl->assign("selected", "");
                            }
                            $xtpl->insert_loop("main.card_modif.isRH.input" . ucfirst($nom). ".option". ucfirst($nom), array());
                        }

                        $xtpl->assign("extension1", $users[$i]->getExtension1());
                        $xtpl->assign("typeCompte", $application->getSelectOptionList()["typeCompte"][$users[$i]->getUserAcCtrl()]);
                        $xtpl->parse("main.card_modif.isRH.input" . ucfirst($nom));
                        if($nom == "typeCompte")
                        {
                            $xtpl->parse("main.card_modif.isRH");
                        }
                    }
                    else
                    {
                        $xtpl->parse("main.card_modif.input" . ucfirst($nom));
                    }
                }
                else
                {
                    if($nom == "typeCompte" || $nom == "extension1")
                    {
                        $xtpl->assign("typeCompte", $application->getSelectOptionList()["typeCompte"][$users[$i]->getUserAcCtrl()]);
                        $xtpl->assign("extension1", $users[$i]->getExtension1());
                        $xtpl->parse("main.card_modif.isRH." . $nom);
                        if($nom == "typeCompte")
                        {
                            $xtpl->parse("main.card_modif.isRH");
                        }
                    }
                    else
                    {
                        $xtpl->parse("main.card_modif." . $nom);
                    }
                }
            }
            $xtpl->assign("samAcName", $users[$i]->getSamAcName());
            $xtpl->insert_loop("main.card_modif", array());
        }
        else
        {
            if(sizeof($application->getCurrentUser()->getPermission()) > 0)
            {
                $xtpl->assign("typeCompte", $application->getSelectOptionList()["typeCompte"][$users[$i]->getUserAcCtrl()]);
                $xtpl->insert_loop("main.cardModifBTN.typeCompte", array());
                $xtpl->assign("extension1", $users[$i]->getExtension1());
                $xtpl->insert_loop("main.cardModifBTN.extension1", array());
            }

            $hideModifBTN = "hidden";
            preg_match("/SI[0-9]{4}/", $users[$i]->getDn(), $siteUser);
            foreach($application->getCurrentUser()->getPermission() as $site)
            {
                if($site == $siteUser[0])
                {
                    $hideModifBTN = "";
                }
            }

            if($users[$i]->getDN() == $application->getCurrentUser()->getDN())
            {
                $hideModifBTN = "";
            }

            $xtpl->assign("hideModifBTN", $hideModifBTN);

            $xtpl->insert_loop("main.cardModifBTN", array());
        }
    }

    $xtpl->assign("hiddenOrderBy", $hiddenOrderBy);
    $xtpl->assign("selectNom", $selectNom);
    $xtpl->assign("selectPrenom", $selectPrenom);
    $xtpl->assign("selectPrenomReverse", $selectPrenomInverse);
    $xtpl->assign("selectNomReverse", $selectNomInverse);
    $xtpl->assign("hiddenSearch", $hiddenSearch);
    $xtpl->assign("error", $error);
    $xtpl->assign("message", $message);
    $xtpl->assign("connexion", $application->getIdUser());
    $xtpl->parse("main");
    $xtpl->out("main");
?>