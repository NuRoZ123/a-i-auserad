<?php

require("myApp.php");
include_once "xtpl/xtemplate.class.php";

$xtpl = new XTemplate("pages/connexion.html");
$error = "";
$fonction = "";
$telephone = "";
$samAcName = $application->getCurrentUser()->getSamAcName();
$idInput = $samAcName . "@a-i-a.fr";

if(isset($_POST))
{
    foreach($_POST as $nom => $value)
    {
        if(!($nom == "id") && !($nom == "pass"))
        {
            $xtpl->assign("nomInfo", $nom);
            $xtpl->assign("info", $value);

            $xtpl->insert_loop("index.saveInfo", array());
        }
    }
}

if(isset($_POST["id"]) && isset($_POST["pass"]))
{
    if($application->isGoodIdentifiant($_POST["id"], $_POST["pass"]) && ($_POST["id"] == $_POST["samAcName"] . "@a-i-a.fr" || sizeof($application->getCurrentUser()->getPermission()) > 0))
    {
        $message = "";
        $error = "";
        foreach($_POST as $nom => $value)
        {
            if(!($nom == "samAcName") && !($nom == "id") && !($nom == "pass"))
            {
                if($application->modification($nom, $value, $_POST["samAcName"], $_POST["id"], $_POST["pass"]))
                {
                    if($message === "")
                    {
                        $message .= $nom;
                    }
                    else
                    {
                        $message .= ", " . $nom;
                    }
                }
                else
                {
                    if($error === "")
                    {
                        $error .= $nom;
                    }
                    else
                    {
                        $error .= ", " . $nom;
                    }
                }
            }
        }

        if(!($message === "") && $error === "")
        {
            $application->setMessage("Modification effectué.");
        }
        else if(!($error === "") && $message === "")
        {
            $application->setError("aucune modification effectuée.");
        }
        else
        {
            $application->setMessage("modification " . $message . " effectué.");
            $application->setError("modification " . $error . " non effectué.");
        }

        header('Location: index.php');
        exit();
    }
    else
    {
        $error = "Identifiant ou mot de passe incorrect";
    }
}

$xtpl->assign("idInput", $idInput);
$xtpl->assign("fonction", $fonction);
$xtpl->assign("telephone", $telephone);
$xtpl->assign("samAcName", $samAcName);
$xtpl->assign("error", $error);
$xtpl->assign("connexion", $application->getIdUser());
$xtpl->parse("index");
$xtpl->out("index");

?>