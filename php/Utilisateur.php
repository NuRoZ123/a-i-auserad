<?php

include_once ("myAD.php");

class Utilisateur
{
    /**
     * permet de récupérer une VCard d'un
     * utilisateur à partir d'un mail
     * @param $mail
     */
    public static function getVCard($mail) : void
    {
        $myActiveDirectory = new myAD();
        $nom = "";
        $prenom = "";
        $messagerie = "";
        $adress = "";
        $ville = "";
        $pays = "";
        $codePostal = "";
        $telephoneId = "";
        $telephone = "";
        $fonction = "";
        $service = "";
        $societe = "";

        $user = $myActiveDirectory->getUserByEmail($mail);

            if (!($user->getNom() == "Aucun nom."))
            {
                $nom = $user->getNom();
            }
            if (!($user->getPrenom() == "Aucun prenom."))
            {
                $prenom = $user->getPrenom();
            }
            if (!($user->getMessagerie() == "Aucune messagerie."))
            {
                $messagerie = $user->getMessagerie();
            }
            if (!($user->getAdress() == "Aucun adresse."))
            {
                $adress = $user->getAdress();
            }
            if (!($user->getVille() == "Aucun ville."))
            {
                $ville = $user->getVille();
            }
            if (!($user->getPays() == "Aucun pays."))
            {
                $pays = $user->getPays();
            }
            if (!($user->getCodePostal() == "Aucun code postal."))
            {
                $codePostal = $user->getcodePostal();
            }
            if (!($user->getTelephoneId() == "Aucun id de télephone."))
            {
                $telephoneId = $user->getTelephoneId();

            }
            if (!($user->getTelephone() == "Aucun télephone.")) {
                $telephone = $user->getTelephone();
            }
            if (!($user->getFonction() == "Aucune fonction.")) {
                $fonction = $user->getFonction();
            }
            if (!($user->getService() == "Aucun service.")) {
                $service = $user->getService();
            }
            if (!($user->getSociete() == "Aucune societe")) {
                $societe = $user->getSociete();
            }

            $nomFile = $user->getNom() . "_" . $user->getPrenom();

            $VCard = 'BEGIN:VCARD
VERSION:2.1
N;LANGUAGE=fr:' . $nom . '; ' . $prenom . ';;;
FN:' . $nom . ' ' . $prenom . '
ORG:' . $societe . ';' . $service . '
TITLE:' . $fonction . '
TEL;WORK, VOICE, CHARSET=Windows-1252:' . $telephone . '
TEL;type=FAX:' . $telephoneId . '
ADR;type=work:;;' . $adress . ';' . $ville . ';;' . $codePostal . ';' . $pays . '
EMAIL;PREF;INTERNET:' . $messagerie . '
X-MS-IMADDRESS:' . $messagerie . '
X-MS-CARDPICTURE;TYPE=JPEG;ENCODING=BASE64:
 /9j/4AAQSkZJRgABAQEAkACQAAD/2wBDAAcFBQYFBAcGBQYIBwcIChELCgkJChUPEAwRGBUa
 GRgVGBcbHichGx0lHRcYIi4iJSgpKywrGiAvMy8qMicqKyr/2wBDAQcICAoJChQLCxQqHBgc
 KioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKir/wAAR
 CACUACcDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAA
 AgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkK
 FhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWG
 h4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl
 5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREA
 AgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYk
 NOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOE
 hYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk
 5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD393C4B6mgHqSeKhc7nOPoKRj2HQUAPef+
 5z7mlj5GT19agAyasD5U59M0AQv882PSikiGWJooAcOMn0FJSn7tJQARjLVLMcRn34psI5pJ
 jyBQA1TtiJ75ook4VVooAVuvFJR2oHWgCeMfLmoD80341YPyR/QVXi5Yn2oASU5fFFNZssT7
 0UAP7U6MfMKb2qWIfyxQATNhAPWo1+WMn1pZjl8egoIJUKvJ70AQUVMI1X755ooABycVKhwv
 HJPNIGQ9F5+lK5KoccUAN8sbsvyT2FNeQjhRt4pI+WJPXFNcguaAGqNzfMaKTGTgUUAWIx8w
 /OlmPQfjSxr8p/KmOy7jxk9OegoAIwQufeo8KvU5PoKkc/u+eOAOKioAC56L8v0oplFAF48J
 z2quBuYe5qeU4jPvUKdSfQUAEh6fiajp0n3senFNoAZRRRQBZmPzAelJH93nuaRzlj9aceE+
 g/nQBCTk5ooPWigBlFFFAFkPuIBFKxXHPc0xOpPoKR+oHoKADYp6foaQx+/5imnrQGI6EigA
 2N25+hoo3nuAfwooAlT7rfhTW++31oooAYetFFFADKKKKAP/2Q==

X-MS-OL-DESIGN;CHARSET=utf-8:<card xmlns="http://schemas.microsoft.com/office/outlook/12/electronicbusinesscards" ver="1.0" layout="left" bgcolor="ffffff"><img xmlns="" align="fit" area="16" use="cardpicture"/><fld xmlns="" prop="name" align="left" dir="ltr" style="b" color="000000" size="10"/><fld xmlns="" prop="org" align="left" dir="ltr" color="000000" size="8"/><fld xmlns="" prop="title" align="left" dir="ltr" color="000000" size="8"/><fld xmlns="" prop="dept" align="left" dir="ltr" color="000000" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="telwork" align="left" dir="ltr" color="d48d2a" size="8"><label align="right" color="626262">Bureau</label></fld><fld xmlns="" prop="email" align="left" dir="ltr" color="d48d2a" size="8"/><fld xmlns="" prop="im" align="left" dir="ltr" color="000000" size="8"><label align="right" color="626262">Mess. instant.</label></fld><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/><fld xmlns="" prop="blank" size="8"/></card>
REV:20210520T123107Z
END:VCARD';
        header("Content-type: text/vcard; charset=utf-8");
        header("Content-Disposition: attachment; filename=".$nomFile.".vcf");
        header("Pragma: public");


        echo $VCard;
    }

    private string $idSession;
    private string $samAcName;
    private string $DN;
    private string $nom;
    private string $prenom;
    private string $messagerie;
    private string $adress;
    private string $ville;
    private string $pays;
    private string $codePostal;
    private string $telephoneId;
    private string $telephone;
    private string $homePhone;
    private string $mobile;
    private string $fonction;
    private string $service;
    private string $societe;
    private string $extension1;
    private string $userAcCtrl;
    private ?Utilisateur $manager;
    private string $DnManager;
    private array $permission;

    /**
     * Utilisateur constructor.
     * @param $resultatAD
     */
    function __construct($resultatAD)
    {
        // Constructor of the Utilisateur class
        $idSession = "";
        $samAcName = "";
        $dn = "";
        $nom = "Aucun nom.";
        $prenom = "Aucun prenom.";
        $messagerie = "Aucune messagerie.";
        $adress = "Aucun adresse.";
        $ville = "Aucun ville.";
        $pays = "Aucun pays.";
        $codePostal = "Aucun code postal.";
        $telephoneId = "Aucun id de télephone.";
        $telephone = "Aucun télephone 1.";
        $homePhone = "Aucun télephone 2.";
        $mobile = "Aucun télephone mobile.";
        $fonction = "Aucune fonction.";
        $service = "Aucun service.";
        $societe = "Aucune societe";
        $extension1 = "Aucune extension1.";
        $userAcCtrl = "";
        $dnManager = "Aucun manager";

        if(isset($resultatAD["samaccountname"][0]))
        {
            $samAcName = $resultatAD["samaccountname"][0];
        }

        if(isset($resultatAD["mailnickname"][0]))
        {
            $idSession = $resultatAD["mailnickname"][0];
        }

        if(isset($resultatAD["distinguishedname"][0]))
        {
            $dn = $resultatAD["distinguishedname"][0];
        }

        if(isset($resultatAD["sn"][0]))
        {
            $nom = $resultatAD["sn"][0];
        }

        if(isset($resultatAD["givenname"][0]))
        {
            $prenom = $resultatAD["givenname"][0];
        }

        if(isset($resultatAD["mail"][0]))
        {
            $messagerie = $resultatAD["mail"][0];
        }

        if(isset($resultatAD["streetaddress"][0]))
        {
            $adress = $resultatAD["streetaddress"][0];
        }

        if(isset($resultatAD["l"][0]))
        {
            $ville = $resultatAD["l"][0];
        }

        if(isset($resultatAD["co"][0]))
        {
            $pays = $resultatAD["co"][0];
        }

        if(isset($resultatAD["postalcode"][0]))
        {
            $codePostal = $resultatAD["postalcode"][0];
        }

        if(isset($resultatAD["ipphone"][0]))
        {
            $telephoneId = $resultatAD["ipphone"][0];
        }

        if(isset($resultatAD["telephonenumber"][0]))
        {
            $telephone = $resultatAD["telephonenumber"][0];
        }

        if(isset($resultatAD["homephone"][0]))
        {
            $homePhone = $resultatAD["homephone"][0];
        }

        if(isset($resultatAD["mobile"][0]))
        {
            $mobile = $resultatAD["mobile"][0];
        }

        if(isset($resultatAD["title"][0]))
        {
            $fonction = $resultatAD["title"][0];
        }

        if(isset($resultatAD["department"][0]))
        {
            $service = $resultatAD["department"][0];
        }

        if(isset($resultatAD["company"][0]))
        {
            $societe = $resultatAD["company"][0];
        }

        if(isset($resultatAD["extensionattribute1"][0]))
        {
            $extension1 = $resultatAD["extensionattribute1"][0];
        }

        if(isset($resultatAD["useraccountcontrol"][0]))
        {
            $userAcCtrl = $resultatAD["useraccountcontrol"][0];
        }

        if(isset($resultatAD["manager"][0]))
        {
            $dnManager = $resultatAD["manager"][0];
        }

        $this->setIdSession($idSession);
        $this->setSamAcName($samAcName);
        $this->setDN($dn);
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setMessagerie($messagerie);
        $this->setAdress($adress);
        $this->setVille($ville);
        $this->setPays($pays);
        $this->setCodePostal($codePostal);
        $this->setTelephoneId($telephoneId);
        $this->setTelephone($telephone);
        $this->setHomePhone($homePhone);
        $this->setMobile($mobile);
        $this->setFonction($fonction);
        $this->setService($service);
        $this->setSociete($societe);
        $this->setExtension1($extension1);
        $this->setUserAcCtrl($userAcCtrl);
        $this->setDnManager($dnManager);
        $this->permission = [];
    }

    /**
     * getter sur l'attribut idSession
     * @return string
     */
    public function getIdSession(): string
    {
        return $this->idSession;
    }

    /**
     * setter sur l'attribut idSession
     * @param string $idSession
     */
    private function setIdSession(string $idSession): void
    {
        $this->idSession = $idSession;
    }

    /**
     * getter sur l'attribut samAcName
     * @return string
     */
    public function getSamAcName(): string
    {
        return $this->samAcName;
    }

    /**
     * setter sur l'attribut samAcName
     * @param string $samAcName
     */
    private function setSamAcName(string $samAcName): void
    {
        $this->samAcName = $samAcName;
    }

    /**
     * getter sur l'attribut DN
     * @return string
     */
    public function getDN(): string
    {
        return $this->DN;
    }

    /**
     * setter sur l'attribut DN
     * @param string $DN
     */
    private function setDN(string $DN): void
    {
        $this->DN = $DN;
    }

    /**
     * getter sur l'attribut nom
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * setter sur l'attribut nom
     * @param string $nom
     */
    private function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    /**
     * getter sur l'attribut prenom
     * @return string
     */
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    /**
     * setter sur l'attribut prenom
     * @param string $prenom
     */
    private function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * getter sur l'attribut messagerie (mail)
     * @return string
     */
    public function getMessagerie(): string
    {
        return $this->messagerie;
    }

    /**
     * setter sur l'attribut messagerie (mail)
     * @param string $messagerie
     */
    private function setMessagerie(string $messagerie): void
    {
        $this->messagerie = $messagerie;
    }

    /**
     * getter sur l'attribut adress
     * @return string
     */
    public function getAdress(): string
    {
        return $this->adress;
    }

    /**
     * setter sur l'attribut adress
     * @param string $adress
     */
    private function setAdress(string $adress): void
    {
        $this->adress = $adress;
    }

    /**
     * getter sur l'attribut ville
     * @return string
     */
    public function getVille(): string
    {
        return $this->ville;
    }

    /**
     * setter sur l'attribut ville
     * @param string $ville
     */
    private function setVille(string $ville): void
    {
        $this->ville = $ville;
    }

    /**
     * getter sur l'attribut pays
     * @return string
     */
    public function getPays(): string
    {
        return $this->pays;
    }

    /**
     * setter sur l'attribut pays
     * @param string $pays
     */
    private function setPays(string $pays): void
    {
        $this->pays = $pays;
    }

    /**
     * getter sur l'attribut codePostal
     * @return string
     */
    public function getCodePostal(): string
    {
        return $this->codePostal;
    }

    /**
     * setter sur l'attribut codePostal
     * @param string $codePostal
     */
    private function setCodePostal(string $codePostal): void
    {
        $this->codePostal = $codePostal;
    }

    /**
     * getter sur l'attribut telephoneId
     * @return string
     */
    public function getTelephoneId(): string
    {
        return $this->telephoneId;
    }

    /**
     * setter sur l'attribut telephoneId
     * @param string $telephoneId
     */
    private function setTelephoneId(string $telephoneId): void
    {
        $this->telephoneId = $telephoneId;
    }

    /**
     * getter sur l'attribut telephone
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * setter sur l'attribut telephone
     * @param string $telephone
     */
    private function setTelephone(string $telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * getter sur l'attribut homePhone
     * @return string
     */
    public function getHomePhone(): string
    {
        return $this->homePhone;
    }

    /**
     * setter sur l'attribut homePhone
     * @param string $homePhone
     */
    public function setHomePhone(string $homePhone): void
    {
        $this->homePhone = $homePhone;
    }

    /**
     * getter sur l'attribut mobile
     * @return string
     */
    public function getMobile(): string
    {
        return $this->mobile;
    }

    /**
     * setter sur l'attribur mobile
     * @param string $mobile
     */
    public function setMobile(string $mobile): void
    {
        $this->mobile = $mobile;
    }

    /**
     * getter sur l'attribut fonction
     * @return string
    */
    public function getFonction(): string
    {
        return $this->fonction;
    }

    /**
     * setter sur l'attribut fonction
     * @param string $fonction
     */
    private function setFonction(string $fonction): void
    {
        $this->fonction = $fonction;
    }

    /**
     * getter sur l'attribut service
     * @return string
     */
    public function getService(): string
    {
        return $this->service;
    }

    /**
     * setter sur l'attribut service
     * @param string $service
     */
    private function setService(string $service): void
    {
        $this->service = $service;
    }

    /**
     * getter sur l'attribut societe
     * @return string
     */
    public function getSociete(): string
    {
        return $this->societe;
    }

    /**
     * setter sur l'attribut societe
     * @param string $societe
     */
    private function setSociete(string $societe): void
    {
        $this->societe = $societe;
    }

    /**
     * getter sur l'attribut extension1
     * @return string
     */
    public function getExtension1(): string
    {
        return $this->extension1;
    }

    /**
     * setter sur l'attribut extension1
     * @param string $extension1
     */
    private function setExtension1(string $extension1): void
    {
        $this->extension1 = $extension1;
    }

    /**
     * getter sur l'attribut getUserAcCtrl
     * @return string
     */
    public function getUserAcCtrl(): string
    {
        return $this->userAcCtrl;
    }

    /**
     * setter sur l'attribut userAcCtrl
     * @param string $userAcCtrl
     */
    private function setUserAcCtrl(string $userAcCtrl): void
    {
        $this->userAcCtrl = $userAcCtrl;
    }

    /**
     * getter sur l'attribut manager
     * @return Utilisateur
     */
    public function getManager(): Utilisateur
    {
        return $this->manager;
    }

    /**
     * setter sur l'attribut manager
     * @param Utilisateur $user
     */
    public function setManager(Utilisateur $user) : void
    {
        $this->manager = $user;
    }

    /**
     * getter sur l'attribut dnManager
     * @return string
     */
    public function getDnManager(): string
    {
        return $this->DnManager;
    }

    /**
     * setter sur l'attribut dnManager
     * @param string $DnManager
     */
    private function setDnManager(string $DnManager): void
    {
        $this->DnManager = $DnManager;
    }

    /**
     * permet de récupérer la VCard d'un utilisateur
     * @return string
     */
    public function getVCardUser(): void
    {
        Utilisateur::getVCard($this->getMessagerie());
    }

    /**
     * getter sur l'attibut hasPermission
     * @return array
     */
    public function getPermission(): array
    {
        return $this->permission;
    }

    /**
     * setter sur l'attribut hasPermission
     */
    public function setPermission(): void
    {
        $droitsList = parse_ini_file("ini/droits.ini", true);
        $groupesList = parse_ini_file("ini/groupes.ini", true)["groupes"];

        foreach($groupesList as $nomPrenom)
        {
            $explodeNomPrenom = explode(" ", $nomPrenom);
            if(($this->getNom() == $explodeNomPrenom[0] && $this->getPrenom() == $explodeNomPrenom[1]) || ($this->getManager()->getNom() == $explodeNomPrenom[0] && $this->getManager()->getPrenom() == $explodeNomPrenom[1]))
            {

                foreach($droitsList as $site => $nomPrenomDroits)
                {
                    foreach($nomPrenomDroits as $nomPrenomUserBySite)
                    {
                        $explodeNomPrenomUserBySite = explode(" ", $nomPrenomUserBySite);
                        if($this->getNom() == $explodeNomPrenomUserBySite[0] && $this->getPrenom() == $explodeNomPrenomUserBySite[1])
                        {
                            array_push($this->permission, $site);
                            break;
                        }
                    }
                }

                break;
            }
        }
    }

    /**
     * permet de récupérer l'objet en liste
     * @return array
     */
    public function getListOfThis()
    {
        return get_object_vars($this);
    }
}

?>
