<?php

class myAD
{
    private string $domaine, $user, $pass;
    private array $nameToADList;

    /**
     * myAD constructor.
     */
    function __construct()
    {
        $ini = parse_ini_file("ini/ADconf.ini", true);
        $this->domaine = $ini["connexion"]["domaine"];
        $this->user = $ini["user"]["id"];
        $this->pass = $ini["user"]["pass"];

        $this->nameToADList = parse_ini_file("ini/nameToAD.ini", true)["converter"];
    }

    /**
     * permet la connexion à l'AD
     * @return Resource
     */
    public function connectAD()
    {
        $connexion = ldap_connect("ldap://" . $this->domaine);
        ldap_set_option($connexion, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connexion, LDAP_OPT_REFERRALS, 0);
        return $connexion;
    }

    /**
     * permet l'authentification à l'AD avec
     * les identifiant du fichier ADconf.ini
     * @param Resource $connectionAD
     * @return bool
     */
    public function authentificationAD($connexionAD): bool
    {
        $res = false;
        if($connexionAD)
        {
            $res = ldap_bind($connexionAD, $this->user, $this->pass);
        }

        return $res;
    }

    /**
     * permet de récupérer tout les utilisateurs
     * de toute les sites
     * @return array
     */
    public function getGlobalUsers(): array
    {
        $connexionAD = $this->connectAD();
        $res = [];
        if($this->authentificationAD($connexionAD))
        {
            $filter = "(objectClass=user)";
            $baseDN = "OU=Utilisateur,OU=Comptes,DC=a-i-a,DC=fr";
            $recherche = ldap_search($connexionAD, $baseDN, $filter) or exit("impossible de rechercher");
            $usersAD = ldap_get_entries($connexionAD, $recherche);
            unset($usersAD["count"]);
            foreach($usersAD as $resultatUser)
            {
                array_push($res, new Utilisateur($resultatUser));
            }
        }
        return $res;
    }

    /**
     * permet de récupérer un utilisateur depuis
     * l'AD à partir de son id de session
     * @param string $idUser
     * @return Utilisateur
     */
    public function getUserByIdSession(string $idUser): Utilisateur
    {
        $res = null;
        $connexionAD = $this->connectAD();
        if($this->authentificationAD($connexionAD))
        {
            $filter = "(&(mailNickname=".$idUser.")(objectClass=user))";
            $baseDN = "OU=Utilisateur,OU=Comptes,DC=a-i-a,DC=fr";
            $recherche = ldap_search($connexionAD, $baseDN, $filter) or exit("impossible de rechercher");
            $res = new Utilisateur(ldap_get_entries($connexionAD, $recherche)[0]);
        }
        return $res;
    }

    /**
     * permet de récupérer un utilisateur depuis
     * l'AD à partir de son DN
     * @param string $dnUser
     * @return Utilisateur
     */
    public function getUserByDn(string $dnUser): Utilisateur
    {
        $res = null;
        $connexionAD = $this->connectAD();
        if($this->authentificationAD($connexionAD))
        {
            $filter = "(objectClass=user)";
            $recherche = ldap_search($connexionAD, $dnUser, $filter) or exit("impossible de rechercher");
            $res = new Utilisateur(ldap_get_entries($connexionAD, $recherche)[0]);
        }
        return $res;
    }

    /**
     * permet de récupérer un utilisateur depuis
     * l'AD à partir de son samAccountName
     * @param string $samAcName
     * @return Utilisateur
     */
    public function getUserBySamAcName(string $samAcName): Utilisateur
    {
        $res = null;
        $connexionAD = $this->connectAD();
        if($this->authentificationAD($connexionAD))
        {
            $filter = "(&(samaccountname=".$samAcName.")(objectClass=user))";
            $baseDN = "OU=Utilisateur,OU=Comptes,DC=a-i-a,DC=fr";
            $recherche = ldap_search($connexionAD, $baseDN, $filter) or exit("impossible de rechercher");
            $res = new Utilisateur(ldap_get_entries($connexionAD, $recherche)[0]);
        }
        return $res;
    }

    /**
     * permet de récupérer un utilisateur depui
     * l'AD à partir de son mail
     * @param string $email
     * @return Utilisateur
     */
    public function getUserByEmail(string $email): Utilisateur
    {
        $res = null;
        $connexionAD = $this->connectAD();
        if($this->authentificationAD($connexionAD))
        {
            $filter = "(&(mail=" . $email . ")(objectClass=user))";
            $baseDN = "OU=Utilisateur,OU=Comptes,DC=a-i-a,DC=fr";
            $recherche = ldap_search($connexionAD, $baseDN, $filter) or exit("impossible de rechercher");
            $res = new Utilisateur(ldap_get_entries($connexionAD, $recherche)[0]);
        }
        return $res;
    }

    /**
     * permet de récupérer des utilisateurs depuis
     * l'AD à partir d'un site
     * @param string $site
     * @return array
     */
    public function getUsersBySite(string $site): array
    {
        $connexionAD = $this->connectAD();
        $res = [];
        if($this->authentificationAD($connexionAD))
        {
            $filter = "(&(objectClass=user)(mail=*))";
            $baseDN = "OU=".$site.",OU=Utilisateur,OU=Comptes,DC=a-i-a,DC=fr";
            $recherche = ldap_search($connexionAD, $baseDN, $filter) or exit("impossible de rechercher");
            $usersAD = ldap_get_entries($connexionAD, $recherche);
            unset($usersAD["count"]);
            foreach($usersAD as $resultatUser)
            {
                array_push($res, new Utilisateur($resultatUser));
            }
        }
        return $res;
    }

    /**
     * permet l'authentification à l'AD
     * avec un identifiant et mot de passe
     * @param string $id
     * @param string $pass
     * @return bool
     */
    public function authentificationWithIdPass($connexionAD, string $id, string $pass): bool
    {
        if($connexionAD)
        {
            $res = @ldap_bind($connexionAD, $id, $pass);
        }
        else
        {
            $res = false;
        }

        return $res;
    }

    /**
     * permet de faire une modification
     * d'attribut à un utilisateur dans l'AD
     * @param string $nom
     * @param string $value
     * @param Utilisateur $user
     * @param string $id
     * @param string $pass
     * @return bool
     */
    public function modificationUser(string $nom, string $value, Utilisateur $user, string $id, string $pass): bool
    {
        $res = false;
        $connexionAD = $this->connectAD();

        if($this->authentificationWithIdPass($connexionAD, $id, $pass))
        {
            foreach($this->nameToADList as $nomList => $nomAD)
            {
                if($nomList == $nom)
                {
                    if($value == "" || $value == " ")
                    {
                        $value = array();
                    }

                    $dn = $user->getDN();
                    $modif[$nomAD] = $value;
                    $res = @ldap_mod_replace($connexionAD, $dn, $modif);
                    break;
                }
            }
        }

        return $res;
    }

    /**
     * permet de désactiver un compte
     * utilisateur de l'ad
     * @param Utilisateur $user
     * @param string $id
     * @param string $pass
     * @return bool
     */
    public function modificationUserDisableAccount(Utilisateur $user, string $id, string $pass) : bool
    {
        $res = false;
        $connexionAD = $this->connectAD();

        if($this->authentificationWithIdPass($connexionAD, $id, $pass))
        {
            $modif["useraccountcontrol"] = $user->getUserAcCtrl() | 2;
            $dn = $user->getDN();
            $res = @ldap_mod_replace($connexionAD, $dn, $modif);
        }

        return $res;
    }

    /**
     * permet de d'activer un compte
     * utilisateur de l'ad
     * @param Utilisateur $user
     * @param string $id
     * @param string $pass
     * @return bool
     */
    public function modificationUserEnableAccount(Utilisateur $user, string $id, string $pass) : bool
    {
        $res = false;
        $connexionAD = $this->connectAD();

        if($this->authentificationWithIdPass($connexionAD, $id, $pass))
        {
            $modif["useraccountcontrol"] = $user->getUserAcCtrl() & ~2;
            $dn = $user->getDN();
            $res = @ldap_mod_replace($connexionAD, $dn, $modif);
        }

        return $res;
    }
}