<?php

include_once "myAD.php";
include "Utilisateur.php";

class myApp
{
    private myAD $myActiveDirectory;
    private Utilisateur $currentUser;
    private array $users, $globalUsers, $modificationList, $selectOptionList;
    private string $error, $message, $idUser;

    /**
     * myApp constructor.
     */
    function __construct()
    {
        $this->myActiveDirectory = new myAD();
        $this->error = "";
        $this->message = "";
        $this->idUser = explode('\\',$_SERVER['REMOTE_USER'])[1];
        // $this->idUser = "a.mathieu";

        $this->modificationList = parse_ini_file("ini/modification.ini", true);
        $this->selectOptionList = parse_ini_file("ini/selectOption.ini", true);

        $this->currentUser = $this->myActiveDirectory->getUserByIdSession($this->idUser);
        $this->currentUser->setManager($this->myActiveDirectory->getUserByDn($this->currentUser->getDnManager()));
        $this->currentUser->setPermission();

        if(!(sizeof($this->currentUser->getPermission()) > 0))
        {
            $this->users = [$this->currentUser];
        }
        else
        {
            $this->users = [];
            foreach($this->currentUser->getPermission() as $site)
            {
                $newUsers = $this->myActiveDirectory->getUsersBySite($site);
                $this->users = array_merge($this->users, $newUsers);
            }
            $this->globalUsers = $this->myActiveDirectory->getGlobalUsers();
        }
    }

    /**
     * getter sur l'attribut currentUser
     * @return Utilisateur
     */
    public function getCurrentUser(): Utilisateur
    {
        return $this->currentUser;
    }

    /**
     * getter sur l'attribut modificationList
     * @return array
     */
    public function getModificationList(): array
    {
        return $this->modificationList;
    }

    /**
     * getter sur l'attribut selectOptinsList
     * @return array
     */
    public function getSelectOptionList() : array
    {
        return $this->selectOptionList;
    }

    /**
     * getter sur l'attribut users
     * @return Utilisateur[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * permet de récupérer les utilisateurs qui correspond
     * une recherche
     * @param string $search
     * @param array $users
     * @return array
     */
    public function getUsersBySearch(string $search, array $users, bool $isGlobal): array
    {
        if(!$search == "")
        {
            $userSearch = array();
            if($isGlobal)
            {
                foreach($this->globalUsers as $user)
                {
                    $res = false;

                    if (stristr($user->getNom(), $search) || stristr($user->getPrenom(), $search) || stristr($user->getMessagerie(), $search) || stristr($user->getAdress(), $search)
                        || stristr($user->getVille(), $search) || stristr($user->getPays(), $search) || stristr($user->getCodePostal(), $search) || stristr($user->getTelephoneId(), $search)
                        || stristr($user->getTelephone(), $search) || stristr($user->getFonction(), $search) || stristr($user->getService(), $search) || stristr($user->getSociete(), $search))
                    {
                        $res = true;
                    }

                    if ($res)
                    {
                        array_push($userSearch, $user);
                    }
                }
            }
            else
            {
                foreach($users as $user)
                {
                    $res = false;

                    if (stristr($user->getNom(), $search) || stristr($user->getPrenom(), $search) || stristr($user->getMessagerie(), $search) || stristr($user->getAdress(), $search)
                        || stristr($user->getVille(), $search) || stristr($user->getPays(), $search) || stristr($user->getCodePostal(), $search) || stristr($user->getTelephoneId(), $search)
                        || stristr($user->getTelephone(), $search) || stristr($user->getFonction(), $search) || stristr($user->getService(), $search) || stristr($user->getSociete(), $search))
                    {
                        $res = true;
                    }

                    if ($res)
                    {
                        array_push($userSearch, $user);
                    }
                }
            }
        }
        else
        {
            $userSearch = $users;
        }

        return $userSearch;
    }

    /**
     * getter sur l'attribut idUser
     * @return string
     */
    public function getIdUser(): string
    {
        return $this->idUser;
    }

    /**
     * permet l'authentification à l'AD
     * avec un identifiant et mot de passe
     * @param string $id
     * @param string $pass
     * @return bool
     */
    public function isGoodIdentifiant(string $id, string $pass): bool
    {
        return $this->myActiveDirectory->authentificationWithIdPass($this->myActiveDirectory->connectAD(), $id, $pass);
    }

    /**
     * permet de modifier le numéro de telephone
     * et la fonction d'un utilisateur
     * @param string $fonction
     * @param string $telephone
     * @param string $id
     * @param string $pass
     */
    public function modification(string $nom, string $valeur, string $samAcName, string $id, string $pass) : bool
    {
        $userModif = null;
        foreach($this->users as $user)
        {
            if($user->getSamAcName() == $samAcName)
            {
                $userModif = $user;
                break;
            }
        }

        $res = false;
        if($nom == "typeCompte")
        {
            if($valeur == "verrouillé")
            {
                $res = $this->myActiveDirectory->modificationUserDisableAccount($userModif, $id, $pass);
            }
            else if($valeur == "déverrouillé")
            {
                $res = $this->myActiveDirectory->modificationUserEnableAccount($userModif, $id, $pass);
            }
        }
        else
        {
            $res = $this->myActiveDirectory->modificationUser($nom, $valeur, $userModif, $id, $pass);
        }

        $this->updateUserBySamAcName($samAcName);

        return $res;
    }

    /**
     * setter sur l'attribut error
     * @param string $value
     */
    public function setError(string $value): void
    {
        $this->error = $value;
    }

    /**
     * getter sur l'attribut error
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    /**
     * getter sur l'attribut message
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * setter sur l'attribut message
     * @param string $value
     */
    public function setMessage(string $value): void
    {
        $this->message = $value;
    }


    /**
     * met à jour un utilisateur dans l'attribut users
     * @param $samAcName
     */
    public function updateUserBySamAcName($samAcName): void
    {
        $newUser = $this->myActiveDirectory->getUserBySamAcName($samAcName);
        if($newUser->getDnManager() != "Aucun manager")
        {
            $newUser->setManager($this->myActiveDirectory->getUserByDn($newUser->getDnManager()));
        }


        for($i = 0; $i < sizeof($this->users); ++$i)
        {
            if($newUser->getSamAcName() == $this->users[$i]->getSamAcName())
            {
                $this->users[$i] = $newUser;
                break;
            }
        }
    }

    /**
     * permet de recuperer les utilisateurs suite
     * a une recheche et la range par nom ou prenom
     * @param $orderBy
     * @param $search
     * @param $isGlobal
     * @return array
     */
    public function usersOrderBy($orderBy, $search, $users, $isGlobal): array
    {
        $users = $this->getUsersBySearch($search, $users, $isGlobal);
        $newOrderList = array();
        if($orderBy == "prenom" || $orderBy == "prenomReverse")
        {
            foreach($users as $user)
            {
                $newOrderList[$user->getDN()] = $user->getPrenom();
            }
        }
        else if($orderBy == "nom" || $orderBy == "nomReverse")
        {
            foreach($users as $user)
            {
                $newOrderList[$user->getDN()] = $user->getNom();
            }
        }

        asort($newOrderList);
        $newUsersList = array();
        foreach($newOrderList as $dn => $nom)
        {
            foreach($users as $user)
            {
                if($user->getDN() == $dn)
                {
                    array_push($newUsersList, $user);
                    break;
                }
            }
        }

        return $newUsersList;
    }

    /**
     * myApp destructor
     */
    function __destruct()
    {
        $_SESSION["application"] = $this;
    }
}

//sauvegarde session
session_start();
session_destroy();
if(isset($_SESSION["application"]))
{
    $application = $_SESSION["application"];
}
else
{
    $application = new myApp();
}